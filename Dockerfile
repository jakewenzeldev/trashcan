
FROM node:16 as frontend-build


WORKDIR /app/frontend

COPY frontend/package*.json ./

RUN npm install

COPY frontend/*.js ./
COPY frontend/src ./src
COPY frontend/public ./public

ARG PREFIX=/
ENV VUE_APP_PUBLIC_URL=$PREFIX
ENV VUE_APP_BACKEND_URL=$PREFIX

RUN npm run build


#####

FROM ubuntu as backend-build

ARG PREFIX=/
ENV api_prefix=$PREFIX

COPY --from=frontend-build /app/frontend/dist /app/frontend/dist/$api_prefix

WORKDIR /app/

RUN apt-get update && \
	apt-get install -y \
		g++ meson ninja-build pkg-config curl git \
		sqlite3 libsqlite3-dev libmagic-dev file  \
		default-jre && \
	mkdir -p /usr/local/share/trashcan/ && \
	curl https://archive.apache.org/dist/tika/tika-app-1.26.jar -o /usr/local/share/trashcan/tika-app.jar

COPY subprojects ./subprojects
COPY scripts ./scripts
COPY meson* config.h* ./
COPY include ./include
COPY src ./src


RUN mkdir build && cd build && \
	meson -Dapi-prefix=$api_prefix .. && \
	ninja && \
	ninja install && \
	apt-get purge -y meson ninja-build pkg-config curl git && \
	rm -rf /app/

CMD /usr/local/bin/trashcan

