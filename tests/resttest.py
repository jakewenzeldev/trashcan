import os
import unittest
import requests
import urllib
import random
import string

class RestTest(unittest.TestCase):
    url = os.environ.get('TC_URL', "http://localhost:8000")

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def _genstr(self):
        letters = string.ascii_letters
        return ''.join(random.choice(letters) for i in range(5))

    def __url(self, path):
        url_parts = list(urllib.parse.urlparse(self.url))
        url_parts[2] = path
        # url_parts[4] = urllib.parse.urlencode(args_dict)
        p = urllib.parse.urlunparse(url_parts)
        return p

    def _get(self, path, params = {}):
        r = requests.get(self.__url(path), params = params)
        return (r.status_code, r.json())

    def _post(self, path, data, file = None):
        multipart = []

        if type(data) == dict:
            for k, v in data.items():
                if type(v) == list:
                    for i in v:
                        multipart.append((k, (None, i)))
                else:
                    multipart.append((k, (None, v)))
        else:
            for k, v in data:
                multipart.append((k, (None, v)))

        if file:
            content = ''
            with open(file, 'r') as s:
                content = s.read()

            multipart.append(('file', (os.path.basename(file), content)))

        r = requests.post(self.__url(path), files = multipart)
        return (r.status_code, r.json())

    def _delete(self, path):
        r = requests.delete(self.__url(path))

        body = ''
        if r.text:
            body = r.json()

        return (r.status_code, body)

