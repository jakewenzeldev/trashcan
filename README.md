# Trash Can

*One man's trash is another man's related work.*

`trashcan` -- web based service for indexing scientific papers (or any other
documents)

Features:

* Single lightweight backend service written in C++ using SQLite database.
* Single page web interface. PDF files can be preview in the same browser window.
* Documents metadata can be fetched using DOI, ArXiv or ISBN (using Google Books)
 identifiers.
* Documents can be categorized using user-defined tags.
* Full-text search support. Documents contents is extracted with Apache Tika
  and indexed using SQLite FTS.

# Screenshots

Search page:

[![img1](docs/img/img1.png)](docs/img/img1.png) 

Edit page:

[![img2](docs/img/img2.png)](docs/img/img2.png) 

Mobile version:

[![img3](docs/img/img3.gif)](docs/img/img3.gif) 

# Deploying with Docker

Build container:

```bash
docker build -t trashcan .
```

If you are going to run the service on non-root URL prefix, for example
`/trash/`, you should specify the `PREFIX` argument:

```bash
docker build --build-arg PREFIX=/trash/ -t trashcan .
```

Start container:

```bash
docker run \
    -p 8000:8000 \
    -v $HOME/trashcan/:/var/lib/trashcan/ \
    -t trashcan
```

`trashcan` service would listen on 8000 port
[(http://localhost:8000/)](http://localhost:8000/) address and store its files
in your `$HOME/trashcan/` directory.

# Deploying with docker-compose

`docker-compose.yaml` with trashcan service may look like this:

```yaml
version: '3.9'
services:
  trashcan:
    build: 'https://gitlab.com/mildlyparallel/trashcan.git'
    restart: always
    ports:
      - 8000:8000
    volumes:
      - /path/to/your/trash/:/var/lib/trashcan/
```

> don't forget to change the path `/path/to/your/trash/` for persisted storage

Then you can build the service image and start it:

```bash
docker compose build
docker compose up
```

The service would run on [(http://localhost:8000/)](http://localhost:8000/)
address and store its files to `/path/to/your/trash/` directory.

# Compiling from source

Requirements:

* meson build system
* sqlite3
* libmagic (libmagic-dev or file-devel packages)
* [Apache Tika App](https://www.apache.org/dyn/closer.cgi/tika/tika-app-1.26.jar)
* nodejs, npm (optional, required only for frontend compilation)

You can download compiled frontend assets archive (`frontend.tar.gz`) from
[GitLab Release page]([https://gitlab.com/mildlyparallel/trashcan/-/releases])
and extract it to the project root. Or, instead, you may compile them manually:

```bash
cd frontend
npm install 
npm run build 
cd ..
```

Configure backend project:

```bash
mkdir build && cd build
meson ..
```

By default, your files and database would be stored in `/var/lib/trashcan/`
directory, to change you can use command line arguments later or compile it
with the corresponding option:

```bash
mkdir build && cd build
meson .. -Ddefault-storage-dir=/path/to/your/trash/
```

Compile and install it:

```bash
meson compile
sudo meson install
```

By default, frontend files and content extraction script would be placed in
`$PREFIX/usr/share` and backend executable would be in `$PREFIX/bin`.

# Usage

You'll need to set path to tika-app jar file to `TIKA_JAR_PATH` environment
variable, or you may create a link path to `tika-app.jar` in the current workdir
(check `script/extract.sh`).

```bash
export TIKA_JAR_PATH=/home/user/tika/tika-app-1.26.jar
# or 
# ln -s /home/user/tika/tika-app-1.26.jar tika-app.jar
```

And then you may start backend service:

```bash
./build/trashcan
```
Run `./build/trashcan -h` to see more configuration options.

## Systemd service

Make sure TIKA jar files is installed to `/usr/share/java/tika-app.jar`, or
change the path in the service file.

```bash
loginctl enable-linger $USER

mkdir -p ~/.config/systemd/user/
cp trashcan.service ~/.config/systemd/user/
systemctl --user enable trashcan
systemctl --user start trashcan
```

## Nginx integration

Nginx can be used as a reverse proxy and for dispatching static content. In
the following configuration nginx would serve static files from `/trash/`
prefix and redirect request coming to `/trash/api/` to the backend service.

To change prefixes for static assets and backend API in the frontend, you'd
need to specify them in `.env.local` file and recompile the frontend:

```bash
cd frontend
cat > .env.local <<EOF
VUE_APP_PUBLIC_URL=/trash/
VUE_APP_BACKEND_URL=/trash/api/
EOF

npm install 
npm run build 
cd ..
```

To specify backend API prefix, you would need to compile it with `api-prefix`
option:

```bash
meson .. -Dapi-prefix=/trash/api/ 
meson compile
sudo meson install
```

Then, in nginx configuration file, you'd need to set `/trash/` location as an
alias to your static content and `/trash/api` as a proxy pass to the backend.
Make sure to increase request size in order for file uploads to work.

```nginx
server {
	listen 80;
	location /trash/ {
		autoindex off;
		alias /usr/local/share/trashcan/dist/;
	}
	location /trash/api/ {
		client_max_body_size 300M;
		proxy_pass http://127.0.0.1:8000;
	}
}
```

