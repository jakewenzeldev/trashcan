import axios from 'axios';

import {Bibtex} from './Bibtex';

const fetchData = async function(isbn) {
	var url = new URL('https://www.googleapis.com/books/v1/volumes');
	url.searchParams.set('q', 'isbn:' + isbn);

	if (!url)
		return {};

	var res = await axios({
		method: "GET",
		url: url,
		headers: {
			"Accept": "application/json"
		}
	});

	if (res.status != 200)
		throw `GET ${url} returned ${res.status}`;

	return res.data;
};

const parseMetadata = function(data) {
	if (!data.items || data.items.length == 0 || !data.items[0].volumeInfo)
		throw 'Nothing is found';

	var vol = data.items[0].volumeInfo;

	var title = vol.title;

	if (vol.subtitle)
		title += '. ' + vol.subtitle;

	var authors = [];
	for (var name of vol.authors) {
		var fields = {
			name: name,
		};

		var tokens = name.split(' ');

		if (tokens.length > 1) {
			fields.firstname = tokens[0];
			fields.surname = tokens.slice(1).join(' ');
			fields.name = fields.surname + ' ' + fields.firstname;
		}

		authors.push(fields);
	}

	var published_year = null;
	var published_month = null;

	var published_arr = vol.publishedDate.split('-');
	if (published_arr.length == 3) {
		published_year = published_arr[0];
		published_month = published_arr[1];
	}

	var metadata = {
		title: title,
		year: published_year,
		authors: (authors.map((v) => v.name)).join(', ')
	};

	metadata.bibtex = Bibtex.getStr('book', {
		title: title,
		authors: authors,
		year: published_year,
		month: published_month,
		publisher: vol.publisher,
		pageCount: vol.pageCount
	});

	return metadata;
};

const GoogleBooks = {
	fetch: async function(isbn) {
		var data = await fetchData(isbn);

		var metadata = parseMetadata(data);

		metadata.isbn = isbn;

		return metadata;
	},
};

export { GoogleBooks };

