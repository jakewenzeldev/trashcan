import Vue from 'vue';
import App from './App.vue';

import vuetify from './plugins/vuetify';
import router from './plugins/router';
import store from './store';

export const bus = new Vue();

import VueSplit from 'vue-split-panel'
Vue.use(VueSplit)

Vue.config.productionTip = false

new Vue({
	vuetify,
	store,
	router,
	render: h => h(App)
}).$mount('#app')
