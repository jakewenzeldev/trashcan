import Vue from 'vue';
import Vuex from 'vuex';

import TagsModule from './TagsModule';
import PapersModule from './PapersModule';
import ViewerModule from './ViewerModule';

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		tags: TagsModule,
		papers: PapersModule,
		viewer: ViewerModule,
	}
});

