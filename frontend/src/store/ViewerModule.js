
const ViewerModule = {
	namespaced: true,

	state: () => ({
		paper: {
			paper_id: 0
		},
		opened: localStorage.getItem('defaultPreviewOpen') == 'true'
	}),

	mutations: {
		open: function(state) {
			state.opened = true;
		},

		close: function(state) {
			state.opened = close;
		},

		toggle: function(state) {
			state.opened = !state.opened;
		},

		preview: function(state, paper) {
			state.paper = paper;
		},
	},
};

export default ViewerModule

