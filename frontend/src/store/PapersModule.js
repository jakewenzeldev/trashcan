
import Vue from 'vue';
import {Papers} from '../api/Papers';

const papers_limit = 50;

const PapersModule = {
	namespaced: true,

	state: {
		papers: [],
		haveMore: true
	},

	mutations: {
		add: function(state, paper) {
			state.papers.push(paper);
		},

		set: function(state, papers) {
			state.papers = papers;
			state.haveMore = papers.length == papers_limit;
		},

		append: function(state, papers) {
			for (var p of papers)
				state.papers.push(p);

			state.haveMore = papers.length == papers_limit;
		},

		check: function(state, args) {
			var paper = state.papers[args.paper_idx];
			paper.checked = args.checked;
			Vue.set(state.papers, args.paper_idx, paper);
		},

		link: function(state, args) {
			for (var p in state.papers) {
				if (state.papers[p].paper_id != args.paper_id)
					continue;

				var paper = state.papers[p];

				for (var l of args.links)
					paper.tags.push(l);

				Vue.set(state.papers, p, paper);
			}
		},

		remove: function(state, paper_id) {
			for (var i in state.papers) {
				if (state.papers[i].paper_id != paper_id)
					continue;

				state.papers.splice(i, 1);
				return;
			}
		},

		setBibtex: function(state, args) {
			var paper = state.papers[args.paper_idx];
			paper.bibtex = args.bibtex;
			Vue.set(state.papers, args.paper_idx, paper);
		},
	},

	actions: {
		checkAll: function(context, checked) {
			for (var p in context.state.papers) {
				context.commit('check', {paper_idx: p, checked: checked});
			}
		},

		find: async function(context, args) {
			var offset = 0;
			if (args.append)
				offset = context.state.papers.length;

			var papers = await Papers.find(papers_limit, offset, args.query, args.tags);

			if (args.append)
				context.commit('append', papers);
			else
				context.commit('set', papers);
		},

		fetch: async function(context) {
			if (context.state.papers && context.state.papers.length > 0)
				return;

			var papers = await Papers.find(papers_limit, 0, '', []);
			context.commit('set', papers);
		},

		link: async function(context, args) {
			var links = await Papers.link(args.paper_id, args.tags_ids);

			var newLinks = [];

			for (var link of links) {
				for (const tag of args.tags) {
					if (link.tag_id != tag.tag_id)
						continue;
					var tag2 = Object.assign({}, tag);

					tag2.paper_id = args.paper_id;
					tag2.link_id = link.link_id;
					newLinks.push(tag2);
				}
			}

			context.commit('link', {
				paper_id: args.paper_id,
				links: newLinks
			});
		},

		remove: async function(context, paper_id) {
			await Papers.remove(paper_id);

			context.commit('remove', paper_id);
		},

		unlink: async function(context, link) {
			await Papers.unlink(link.link_id);

			for (var paper of context.state.papers) {
				if (paper.paper_id != link.paper_id)
					continue;
				for (var i in paper.tags) {
					if (paper.tags[i].link_id != link.link_id)
						continue;
					paper.tags.splice(i, 1);
					return;
				}
			}
		},

		fetchBibtex: async function(context) {
			var ids = [];

			for (var p of context.state.papers) {
				if (p.checked)
					ids.push(p.paper_id);
			}

			var bmap = await Papers.bibtex(ids);

			for (const bibtex of bmap) {
				for (const i in context.state.papers) {
					if (context.state.papers[i].paper_id != bibtex.paper_id)
						continue;

					context.commit('setBibtex', {
						paper_idx: i,
						bibtex: bibtex.bibtex
					});

					break;
				}
			}

			return bmap;
		},

		fetchOne: async function(context, paper_id) {
			return await Papers.findOne(paper_id);
		},

		update: async function(context, paper) {
			await Papers.update(paper);
		},

		create: async function(context, args) {
			return await Papers.create(args.paper, args.file);
		},
	},

	getters: {
		empty: state => {
			return !state.papers || state.papers.length == 0;
		},

		last_paper_id: state => {
			if (!state.papers || state.papers.length == 0) {
				return null;
			}
			var n = state.papers.length;
			var i = state.papers[n - 1].paper_id;
			return i;
		},

		someChecked: state => {
			for (var p of state.papers) {
				if (p.checked)
					return true;
			}

			return false;
		},

		checked: state => {
			var ret = [];

			for (var p of state.papers) {
				if (p.checked)
					ret.push(p);
			}

			return ret;
		},
	}

};

export default PapersModule

