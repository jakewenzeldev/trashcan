#!/bin/bash

set -ex

input=${1:-favicon.svg}

sizes="16 32 48 64 128"

for s in $sizes
do
	inkscape -w $s -h $s -o size-$s.png $input 
done

pnglist="$(printf "size-%s.png " $sizes)"

convert $pnglist favicon.ico

identify favicon.ico

rm -rf $pnglist

