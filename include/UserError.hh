#pragma once

#include <cstddef>
#include <cassert>
#include <ostream>
#include <stdexcept>

class UserError : std::exception
{
public:
	enum Error : uint64_t {
		Ok = 0,
		InvalidId,
		InvalidValue,
		FileNotExists,
		MissingFields,

		nr_codes
	};

	static const constexpr char *message[Error::nr_codes] = {
		"No error",
		"Invalid identifier",
		"Invalid value",
		"File does not exists",
		"Required fields are empty"
	};

	UserError(Error code)
	: m_error(code)
	{ }

	Error error() const
	{
		return m_error;
	}

	const char* what() const noexcept override
	{
		return message[m_error];
	}

protected:
	Error m_error;
};

