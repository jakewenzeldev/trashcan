#pragma once

#include <cassert>
#include <utility>
#include <cstddef>
#include <iostream>

#include "Statement.hh"
#include "Database.hh"
#include "Serializer.hh"
#include "UserError.hh"
#include "Utils.hh"

class Papers
{
public:
	enum Field {
		PaperId = 0,
		Title,
		FirstAuthor,
		Year,
		Doi,
		Isbn,
		Arxiv,
		Bibtex,
		Url,
		Notes,
		Filename,
		Filesize,
		Filemime,
		CreatedAt,
		UpdatedAt,
		IndexedAt,
		Authors,
		Content,

		nr_fields
	};

	static const constexpr char *names[nr_fields] = {
		[PaperId]     = "paper_id",
		[Title]       = "title",
		[FirstAuthor] = "first_author",
		[Year]        = "year",
		[Doi]         = "doi",
		[Isbn]        = "isbn",
		[Arxiv]       = "arxiv",
		[Bibtex]      = "bibtex",
		[Url]         = "url",
		[Notes]       = "notes",
		[Filename]    = "filename",
		[Filesize]    = "filesize",
		[Filemime]    = "filemime",
		[CreatedAt]   = "created_at",
		[UpdatedAt]   = "updated_at",
		[IndexedAt]   = "indexed_at",
		[Authors]     = "authors",
		[Content]     = "content",
	};

	template <typename T>
	using Fields = std::array<T, nr_fields>;

	inline static const char *name(size_t i) {
		assert(i < nr_fields);
		return names[i];
	}

	inline static size_t size() {
		return nr_fields;
	}

	template <typename T>
	inline static std::pair<const char *, T> make_pair(Field f, const T &value) {
		return std::make_pair(name(f), value);
	}

	struct Create : public Statement {
		static const constexpr char *sql =
			"CREATE TABLE IF NOT EXISTS Papers ( "
				"paper_id INTEGER PRIMARY KEY, "
				"title TEXT, "
				"first_author TEXT, "
				"authors TEXT, "
				"year INTEGER, "
				"filename TEXT, "
				"filesize INTEGER, "
				"filemime TEXT, "
				"doi TEXT, "
				"isbn TEXT, "
				"arxiv TEXT, "
				"bibtex TEXT, "
				"url TEXT, "
				"notes TEXT, "
				"created_at INTEGER DEFAULT (strftime('%s', 'now')), "
				"updated_at INTEGER DEFAULT (strftime('%s', 'now')), "
				"indexed_at INTEGER "
			");";

		Create(Database &db)
		: Statement(db, sql)
		{ }
	};

	struct PaperFields : public Statement {
		PaperFields(Database &db, const char *sql)
		: Statement(db, sql)
		{ }

		uint64_t paper_id = 0;
		std::string_view title;
		std::string_view first_author;
		std::string_view authors;
		uint64_t year = 0;
		std::string_view doi, arxiv, isbn;
		std::string_view bibtex;
		std::string_view url;
		std::string_view notes;
		std::string_view filename;
		uint64_t filesize = 0;
		std::string_view filemime;

		inline void write(Serializer &s) {
			assert(paper_id > 0);
			s.write_object(make_pair(PaperId, paper_id));
		}
	};

	struct Insert : public PaperFields {
		static const constexpr char *sql =
			"INSERT INTO Papers ( "
				"title, "
				"first_author, "
				"authors, "
				"year, "
				"doi, "
				"isbn, "
				"arxiv, "
				"bibtex, "
				"url, "
				"notes, "
				"filename, "
				"filesize, "
				"filemime "
			") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

		Insert(Database &db)
		: PaperFields(db, sql)
		, m_db(db)
		{ }

		inline void run()
		{
			if (title.empty() && filename.empty())
				throw UserError(UserError::MissingFields);

			std::string fn;

			if (filesize > 0) {
				assert(!filename.empty());
				assert(!filemime.empty());

				if (!first_author.empty() && year > 0 && !title.empty()) {
					fn += utils::first_word(first_author);
					fn += std::to_string(year);
					fn += utils::first_word(title);
				} else {
					fn = filename;
				}
			}

			if (title.empty())
				title = filename;

			bind_to(0, title);
			bind_to(1, first_author);
			bind_to(2, authors);
			bind_to(3, year);
			bind_to(4, doi);
			bind_to(5, isbn);
			bind_to(6, arxiv);
			bind_to(7, bibtex);
			bind_to(8, url);
			bind_to(9, notes);
			bind_to(10, fn);
			bind_to(11, filesize);
			bind_to(12, filemime);

			step();

			paper_id = m_db.get_rowid();
		}

	private:
		Database &m_db;
	};

	struct Update : public PaperFields {
		static const constexpr char *sql =
			"UPDATE Papers SET "
				"title = ?, "
				"first_author = ?, "
				"authors = ?, "
				"year = ?, "
				"doi = ?, "
				"isbn = ?, "
				"arxiv = ?, "
				"bibtex = ?, "
				"url = ?, "
				"notes = ?, "
				"updated_at = (strftime('%s', 'now')) "
			"WHERE paper_id = ?;";

		Update(Database &db)
		: PaperFields(db, sql)
		{ }

		inline void run()
		{
			assert(paper_id > 0);
			if (title.empty())
				throw UserError(UserError::MissingFields);

			bind_to(0, title);
			bind_to(1, first_author);
			bind_to(2, authors);
			bind_to(3, year);
			bind_to(4, doi);
			bind_to(5, isbn);
			bind_to(6, arxiv);
			bind_to(7, bibtex);
			bind_to(8, url);
			bind_to(9, notes);
			bind_to(10, paper_id);

			step();
		}
	};

	struct UpdateFile: public Statement {
		static const constexpr char *sql =
			"UPDATE Papers SET "
				"filename = ?, "
				"filesize = ?, "
				"filemime = ? "
			"WHERE paper_id = ?;";

		UpdateFile(Database &db)
		: Statement(db, sql)
		{ }

		uint64_t paper_id;
		std::string_view filename;
		uint64_t filesize;
		std::string_view filemime;

		inline void run()
		{
			assert(filesize > 0);
			assert(!filename.empty());
			assert(!filemime.empty());

			bind_to(0, filename);
			bind_to(1, filesize);
			bind_to(2, filemime);
			bind_to(3, paper_id);

			step();
		}
	};

	struct Delete : public Statement {
		static const constexpr char *sql =
			"DELETE FROM Papers WHERE paper_id = ?;";

		Delete(Database &db)
		: Statement(db, sql)
		{ }

		uint64_t paper_id = 0;

		inline void run() {
			assert(paper_id > 0);
			bind_to(0, paper_id);
			step();
		}
	};

	struct SelecFiledata : public Statement {
		static const constexpr char *sql =
			"SELECT filename, filemime, filesize FROM Papers WHERE paper_id = ?;";

		SelecFiledata(Database &db)
		: Statement(db, sql)
		{ }

		std::string filename;
		std::string filemime;
		uint64_t filesize = 0;

		inline void bind(uint64_t paper_id) {
			assert(paper_id > 0);
			bind_to(0, paper_id);
		}

		inline bool load()
		{
			if (!step())
				return false;

			load_at(0, filename);
			load_at(1, filemime);
			load_at(2, filesize);

			return true;
		}
	};

	struct SelectBibtex : public Statement {
		static const constexpr char *sql_part_1 =
			"SELECT paper_id, bibtex "
			"FROM Papers "
			"WHERE paper_id IN (";
		static const constexpr char *sql_part_2 =
			");";

		SelectBibtex(Database &db, const char *sql)
		: Statement(db, sql)
		{ }

		uint64_t paper_id;
		std::string bibtex;

		bool run()
		{
			if (!step())
				return false;

			load_at(0, paper_id);
			load_at(1, bibtex);
			return true;
		}

		inline void write_all(Serializer &s) {
			s.push_list();

			while (run())
				write(s);

			s.pop();
		}

		void write(Serializer &s)
		{
			s.write_object(
				make_pair(PaperId, paper_id),
				make_pair(Bibtex, bibtex)
			);
		}

	};

	struct UpdateIndexDate : public Statement {
		static const constexpr char *sql =
			"UPDATE Papers SET indexed_at = (strftime('%s', 'now')) WHERE paper_id = ?; ";

		UpdateIndexDate(Database &db)
		: Statement(db, sql)
		{ }

		uint64_t paper_id;

		inline void run()
		{
			assert(paper_id > 0);

			bind_to(0, paper_id);

			step();
		}
	};

};

