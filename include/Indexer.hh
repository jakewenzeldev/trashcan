#pragma once

#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <exception>
#include <atomic>

class Config;
class Database;

class Indexer
{
public:
	Indexer(const Config &config, Database &db);

	~Indexer();

	bool push(uint64_t paper_id);

	void spawn();

	void join();

	static void ignore_sigchld();

private:
	void worker_loop();

	uint64_t take_next_id();

	void process_paper(uint64_t paper_id);

	std::string get_content(uint64_t paper_id);

	void index_content(uint64_t paper_id, const std::string &content);

	const Config &m_config;

	Database &m_db;

	std::thread m_worker;

	std::mutex m_queue_lock;
	std::condition_variable m_queue_cv;
	std::queue<uint64_t> m_queue;

	std::atomic_bool m_stopped;

	std::exception_ptr m_worker_ex;
};

