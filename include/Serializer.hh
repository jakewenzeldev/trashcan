#pragma once

#include <sstream>
#include <iomanip>
#include <cfloat>
#include <cmath>
#include <bitset>
#include <string>
#include <cassert>

class Serializer
{
public:
	static const constexpr size_t max_depth = 8;

	Serializer(size_t reserve = 0)
	: m_depth(0)
	, m_dirty(0)
	{
		if (reserve)
			m_out.reserve(reserve);
	}

	inline
	void push_object()
	{
		assert(m_state[m_depth] != Object);

		write_comma();

		m_out += '{';

		push_state(Object);
	}

	inline
	void push_list()
	{
		assert(m_state[m_depth] != Object);

		write_comma();

		m_out += '[';

		push_state(List);
	}

	inline 
	void write(uint64_t v)
	{
		assert(m_state[m_depth] != Object);

		write_comma();

		m_out += std::to_string(v);
	}

	inline 
	void write(double v)
	{
		assert(m_state[m_depth] != Object);

		write_comma();

		if (std::isfinite(v)) {
			m_out += std::to_string(v);
		} else {
			m_out += '"';
			std::to_string(v);
			m_out += '"';
		}
	}

	inline
	void write(const std::string &v)
	{
		assert(m_state[m_depth] != Object);

		write_comma();

		m_out += '"';

		for (auto const &c : v) {
			if (c == '"')
				m_out += "\\\"";
			else if (c == '\\')
				m_out += "\\\\";
			else if (c == '\b')
				m_out += "\\b";
			else if (c == '\f')
				m_out += "\\f";
			else if (c == '\n')
				m_out += "\\n";
			else if (c == '\r')
				m_out += "\\r";
			else if (c == '\t')
				m_out += "\\t";
			else if ('\x00' <= c && c <= '\x1f') {
				// FIXME:
				std::stringstream ss;
				ss << "\\u" << std::hex << std::setw(4) << std::setfill('0') << (int)c;
				m_out += ss.str();
			} else
				m_out += c;
		}

		m_out += '"';
	}

	inline
	void push_key(const char *k)
	{
		assert(m_state[m_depth] == Object);

		write_comma();

		m_out += '"';
		m_out += k;
		m_out += "\":";

		push_state(Key);
	}

	template <typename ...Args>
	void write_key_list(const char *key, Args... args) {
		push_key(key);
		write_list(args...);
		pop();
	}

	template <typename ...Args>
	void write_list(Args... args) {
		push_list();

		int dummy[] = { (write(args), 0)...  };
		(void) dummy;

		pop();
	}

	template <typename T>
	void write_key_value(const char *k, const T &value)
	{
		push_key(k);
		write(value);
		pop();
	}

	template <typename Arg, typename ...Args>
	void write_key_value_seq(
		std::pair<const char *, Arg> kv,
		std::pair<const char *, Args> ...args
	) {
		write_key_value(kv.first, kv.second);
		int dummy[] = { (write_key_value(args.first, args.second), 0)...  };
		(void) dummy;
	}


	template <typename ...Args>
	void write_object(std::pair<const char *, Args> ...args) {
		push_object();
		int dummy[] = { (write_key_value(args.first, args.second), 0)...  };
		(void) dummy;
		pop();
	}

	template <typename ...Args>
	void write_key_object(const char *k, std::pair<const char *, Args> ...args) {
		push_key(k);
		write_object(args...);
		pop();
	}

	inline
	void pop()
	{
		if (m_state[m_depth] == List)
			m_out += ']';
		else if (m_state[m_depth] == Object)
			m_out += '}';

		assert(m_depth > 0);
		m_depth--;
	}

	const std::string &get() const {
		return m_out;
	}

	void reserve(size_t s) {
		m_out.reserve(s);
	}

private:
	enum State {
		List,
		Object,
		Key
	};

	inline
	void write_comma() {
		if (m_dirty[m_depth])
			m_out += ',';
		m_dirty[m_depth] = true;
	}

	inline
	void push_state(State state) {
		m_depth++;
		assert(m_depth < max_depth);

		m_state[m_depth] = state;
		m_dirty[m_depth] = false;
	}

	std::string m_out;

	size_t m_depth;

	std::array<State, max_depth> m_state;

	std::bitset<max_depth> m_dirty;
};

