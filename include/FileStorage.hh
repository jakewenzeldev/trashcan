#pragma once

#include <string>
#include <string_view>
#include <filesystem>

class Config;

class FileStorage
{
public:
	FileStorage(const Config &config);

	bool exists(uint64_t id) const;

	void add(uint64_t id, const std::string_view &buffer);

	bool remove(uint64_t id);

	std::filesystem::path get_path(uint64_t id) const;

protected:

private:
	std::filesystem::path m_storage_dir;
};
