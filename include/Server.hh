#pragma once

#include <memory>
#include <string_view>
#include <optional>

struct mg_connection;
struct mg_mgr;

class Request;
class Response;

class Server
{
public:
	Server();
	
	virtual ~Server();

	void run();

	static void set_exit_signals(std::initializer_list<uint32_t> signals);


protected:
	void serve_static(const Request &request);

	virtual void dispatch(const Request &request) = 0;

	void listen(const std::string &url);

	void poll_loop(int delay_ms);

private:
	static void message_callback(mg_connection *c, int ev, void *ev_data, void *fn_data);

	static void signal_handler(int signo);

	inline static int s_signo = 0;

	std::unique_ptr<struct mg_mgr> m_mongoose;
};
