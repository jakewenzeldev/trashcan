#pragma once

#include "Server.hh"
#include "config.h"

#include "Request.hh"

class Config;
class Database;
class Magic;
class Indexer;
class FileStorage;

class TrashServer : public Server
{
public:
	// FIXME: this sucks
	TrashServer(
		const Config &config,
		Database &database,
		const Magic &magic,
		Indexer &indexer,
		FileStorage &storage
	);
	
	virtual ~TrashServer();

	void run();

protected:

private:
	void find_papers_latest(const Request &req);

	void find_paper(const Request &req);

	void create_paper(const Request &req);

	void update_paper(const Request &req);

	void delete_paper(const Request &req);

	void get_paper_file(const Request &req);

	void get_paper_bibtex(const Request &req);

	void update_paper_file(const Request &req);

	void update_paper_index(const Request &req);

	void add_paper_tags(const Request &req);

	void find_tags_all(const Request &req);

	void create_tag(const Request &req);

	void remove_tag(const Request &req);

	void find_papers(const Request &req);

	void unlink_tag(const Request &req);

	using RouterFunc = void (TrashServer::*)(const Request &);

	struct Route {
		Request::Method method;
		const char *uri;
		RouterFunc func;
	};

	static const constexpr Route routes[] = {
		{ Request::Get,     TC_API_PREFIX "tags",              &TrashServer::find_tags_all },
		{ Request::Post,    TC_API_PREFIX "tags",              &TrashServer::create_tag },
		{ Request::Delete,  TC_API_PREFIX "tags/*",            &TrashServer::remove_tag },
		{ Request::Delete,  TC_API_PREFIX "link/*",            &TrashServer::unlink_tag },
		{ Request::Get,     TC_API_PREFIX "papers",            &TrashServer::find_papers_latest },
		{ Request::Post,    TC_API_PREFIX "papers",            &TrashServer::create_paper },
		{ Request::Post,    TC_API_PREFIX "papers/query",      &TrashServer::find_papers },
		{ Request::Post,    TC_API_PREFIX "papers/bibtex",     &TrashServer::get_paper_bibtex },
		{ Request::Get,     TC_API_PREFIX "papers/*",          &TrashServer::find_paper },
		{ Request::Post,    TC_API_PREFIX "papers/*",          &TrashServer::update_paper },
		{ Request::Delete,  TC_API_PREFIX "papers/*",          &TrashServer::delete_paper },
		{ Request::Get,     TC_API_PREFIX "papers/*/file",     &TrashServer::get_paper_file },
		{ Request::Post,    TC_API_PREFIX "papers/*/file",     &TrashServer::update_paper_file },
		{ Request::Post,    TC_API_PREFIX "papers/*/index",    &TrashServer::update_paper_index },
		{ Request::Post,    TC_API_PREFIX "papers/*/tags",     &TrashServer::add_paper_tags },
	};

	virtual void dispatch(const Request &req) final;

	inline
	uint64_t get_id(const std::string_view &s) {
		return get_id(std::string(s));
	}

	inline
	uint64_t get_id(const Request &req, size_t pos) {
		return get_id(req.get_uri_var(pos));
	}

	uint64_t get_id(const std::string &s);

	const Config &m_config;
	Database &m_db;
	const Magic &m_magic;
	Indexer &m_indexer;
	FileStorage &m_storage;
};
