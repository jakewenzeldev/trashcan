#pragma once

#include <array>
#include <string_view>
#include <set>
#include <string>

#include "Config.hh"
#include "Serializer.hh"

#include "orm/PapersSelect.hh"

class Database;

class Search
{
public:
	Search(Database &server);

	void find_one(size_t paper_id);

	void find_latest();

	void find_by_tags(const std::set<uint64_t> &tags);

	void find_by_query(const std::string_view &query);

	void find_by_tags_and_query(
		const std::set<uint64_t> &tags,
		const std::string_view &query
	);

	void set_limit_offset(uint64_t limit, uint64_t offset);

	const std::string &get() const;

protected:

private:
	void get_papers_list(PapersSelect::Select &stmt);

	void get_paper_tag_list(uint64_t paper_id);

	void get_paper_tag_list(
		const std::array<uint64_t, Config::papers_select_limit> &papers_ids
	);

	Database &m_db;

	uint64_t m_limit;

	uint64_t m_offset;

	Serializer m_output;
};

