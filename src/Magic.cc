#include <stdexcept>
#include "Logger.hh"

#include "Magic.hh"

#include <magic.h>

Magic::Magic()
{ 
	m_cookie = magic_open(MAGIC_MIME_TYPE);

	if (m_cookie == nullptr)
		throw std::runtime_error("Can not initialize magic database");
}

Magic::~Magic()
{ 
	magic_close(m_cookie);
}

void Magic::load()
{
	logger.debug("Loaging magic database");

	if (magic_load(m_cookie, nullptr) == 0)
		return;

	logger.error("Cannot load magic database:", magic_error(m_cookie));
	throw std::runtime_error("Can not load magic database");
}

const char *Magic::mime(const std::string_view &buf) const
{
	return magic_buffer(m_cookie, buf.data(), buf.size());
}

