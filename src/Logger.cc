#include <sstream>
#include <cstring>
#include <iostream>
#include <stdexcept>

#include "Logger.hh"

Logger logger;

Logger::Logger()
: m_level(Level::Info)
, m_out(&std::clog)
{ }

Logger::Level Logger::get_level() const
{
	return m_level;
}

void Logger::set_level(Logger::Level level)
{
	m_level = level;
}

void Logger::set_output_file(const std::string &p)
{
	m_file_stream = std::ofstream(p, std::ios_base::out | std::ios_base::app);

	if (!m_file_stream.good())
		throw std::runtime_error("Can not write to the log file");

	m_out = &m_file_stream;
}

