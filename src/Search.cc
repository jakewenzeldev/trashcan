#include "Search.hh"

#include "Logger.hh"
#include "Utils.hh"
#include "Config.hh"
#include "Database.hh"
#include "Transaction.hh"
#include "UserError.hh"

#include "orm/TagLinks.hh"

static std::ostream &operator<<(std::ostream &os, const std::set<uint64_t> &cont);

static std::ostream &operator<<(
	std::ostream &os,
	const std::array<uint64_t, Config::papers_select_limit> &cont
);

Search::Search(Database &db)
: m_db(db)
, m_limit(Config::papers_select_limit)
, m_offset(0)
{ }

void Search::find_one(size_t paper_id)
{
	PapersSelect::SelectOne stmt(m_db);

	stmt.bind(paper_id);

	get_papers_list(stmt);
}

void Search::find_latest() {
	PapersSelect::SelectAny stmt(m_db);

	stmt.bind(m_limit, m_offset);
	get_papers_list(stmt);
}

void Search::find_by_tags(const std::set<uint64_t> &tags)
{
	assert(!tags.empty());

	std::stringstream qss;
	qss << PapersSelect::SelectByTags::sql_part_1;
	qss << tags;
	qss << PapersSelect::SelectByTags::sql_part_2;

	PapersSelect::SelectByTags stmt(m_db, qss.str().c_str());

	stmt.bind(m_limit, m_offset);

	get_papers_list(stmt);
}

void Search::find_by_query(const std::string_view &query)
{
	assert(!query.empty());
	
	PapersSelect::Match stmt(m_db);

	stmt.bind(query, m_limit, m_offset);

	get_papers_list(stmt);
}

void Search::find_by_tags_and_query(
	const std::set<uint64_t> &tags,
	const std::string_view &query
) {
	assert(!tags.empty());

	std::stringstream qss;
	qss << PapersSelect::MatchTags::sql_part_1;
	qss << tags;
	qss << PapersSelect::MatchTags::sql_part_2;

	PapersSelect::MatchTags stmt(m_db, qss.str().c_str());

	stmt.bind(query, m_limit, m_offset);

	get_papers_list(stmt);
}

void Search::get_papers_list(PapersSelect::Select &stmt)
{
	m_output.reserve(Config::resp_buf_size_start_med);

	m_output.push_object();
	m_output.push_key("papers");

	m_output.push_list();

	size_t nr_papers = 0;
	std::array<uint64_t, Config::papers_select_limit> papers_ids = {0};

	while (stmt.load()) {
		stmt.write(m_output);

		assert(nr_papers < papers_ids.size());
		papers_ids[nr_papers] = stmt.paper_id;
		nr_papers++;

		if (nr_papers == 2)
			m_output.reserve(Config::resp_buf_size_start_large);
	}

	// list
	m_output.pop();

	// key papers 
	m_output.pop();

	m_output.push_key("tags");

	if (nr_papers == 1) {
		get_paper_tag_list(papers_ids[0]);
	} else if (nr_papers > 1) {
		get_paper_tag_list(papers_ids);
	} else {
		m_output.push_list();
		m_output.pop();
	}

	// key tags
	m_output.pop();

	// glob object
	m_output.pop();
}

static std::ostream &operator<<(std::ostream &os, const std::set<uint64_t> &cont)
{
	bool comma = false;
	for (auto &x : cont) {
		if (comma)
			os << ", ";
		comma = true;
		os << x;
	}

	return os;
}

void Search::set_limit_offset(uint64_t limit, uint64_t offset)
{
	if (limit)
		m_limit = utils::min(limit, Config::papers_select_limit);
	else
		m_limit = Config::papers_select_limit;

	m_offset = offset;
}

void Search::get_paper_tag_list(uint64_t paper_id)
{
	assert(paper_id > 0);

	TagLinks::SelectOneTag tag(m_db);
	tag.paper_id = paper_id;
	tag.bind();
	tag.write_all(m_output);
}

void Search::get_paper_tag_list(
	const std::array<uint64_t, Config::papers_select_limit> &papers_ids
) {
	assert(!papers_ids.empty());

	std::stringstream qss;
	qss << TagLinks::SelectAllTags::sql_part_1;
	qss << papers_ids;
	qss << TagLinks::SelectAllTags::sql_part_2;

	TagLinks::SelectAllTags tags(m_db, qss.str().c_str());
	tags.write_all(m_output);
}

static std::ostream &operator<<(
	std::ostream &os,
	const std::array<uint64_t, Config::papers_select_limit> &cont
) {
	bool comma = false;
	for (auto &x : cont) {
		if (!x)
			break;
		if (comma)
			os << ", ";
		comma = true;
		os << x;
	}

	return os;
}

const std::string &Search::get() const
{
	return m_output.get();
}

