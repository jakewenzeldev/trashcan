#include <iostream>
#include <string>
#include <cstring>
#include <cassert>
#include <stdexcept>

#include <signal.h>

#include "Server.hh"
#include "Logger.hh"
#include "UserError.hh"
#include "Request.hh"
#include "Config.hh"

extern "C" {
#include <mongoose.h>
}

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

#define THROW_IF(cond) {\
	if ((cond)) \
		throw std::system_error(errno, std::generic_category(), __FILE__ ":" STR(__LINE__)); \
} while(false);

Server::Server()
: m_mongoose(std::make_unique<struct mg_mgr>())
{
	mg_mgr_init(m_mongoose.get());
}

Server::~Server()
{
	mg_mgr_free(m_mongoose.get());
}

void Server::message_callback(struct mg_connection *connection, int ev, void *ev_data, void *fn_data)
{
	if (ev != MG_EV_HTTP_MSG)
		return;

	assert(connection);
	assert(ev_data);
	assert(fn_data);

	struct mg_http_message *hm = (struct mg_http_message *) ev_data;

	Request request(connection, hm);

	std::string_view method = request.method_str();
	std::string_view uri = request.uri();

	logger.verbose(method, uri, request.body().size(), "bytes");

	Server *ctx = reinterpret_cast<Server *>(fn_data);

	try {
		ctx->dispatch(request);
	} catch (UserError &ex) {
		logger.warning("UserError during", method, uri, "what():", ex.what());
		request.reply_error(400, ex.what());
	} catch (std::exception &ex) {
		logger.error("Exception during", method, uri, "what():", ex.what());
		request.reply_error(500, ex.what());

#ifndef NDEBUG
		std::exit(EXIT_FAILURE);
#endif
	}
}

void Server::listen(const std::string &url)
{
	assert(!url.empty());

	logger.info("Listening at", url, "prefix:", TC_API_PREFIX);

	mg_http_listen(m_mongoose.get(), url.c_str(), &Server::message_callback, this);
}

void Server::poll_loop(int delay_ms)
{
	while (!s_signo) {
		mg_mgr_poll(m_mongoose.get(), delay_ms);
	}

	logger.info("Exiting on signal", strsignal(s_signo));
}

void Server::signal_handler(int signo)
{
	s_signo = signo;
}

void Server::set_exit_signals(std::initializer_list<uint32_t> signals)
{
	int rc;

	struct sigaction sact = {};
	sact.sa_flags = 0;
	sact.sa_handler = &Server::signal_handler;

	rc = sigemptyset(&sact.sa_mask);
	THROW_IF(rc < 0);

	for (auto s : signals) {
		rc = sigaddset(&sact.sa_mask, s);
		THROW_IF(rc < 0);
	}

	for (auto s : signals) {
		logger.debug("Setting handler for", strsignal(s));

		rc = sigaction(s, &sact, nullptr);
		THROW_IF(rc < 0);
	}
}

