#include <cassert>
#include <stdexcept>
#include <cstdio>
#include <cstring>

#include <signal.h>

#include "Indexer.hh"

#include "Database.hh"
#include "Transaction.hh"
#include "Config.hh"
#include "Logger.hh"
#include "Utils.hh"

#include "orm/Papers.hh"
#include "orm/PapersIndex.hh"

Indexer::Indexer(const Config &config, Database &db)
: m_config(config)
, m_db(db)
, m_worker_ex(nullptr)
{  }

Indexer::~Indexer()
{
	try {
		m_stopped = true;
		m_queue_cv.notify_all();
		if (m_worker.joinable())
			m_worker.join();
	} catch (...) { }
}

void Indexer::spawn()
{
	m_stopped = false;

	m_worker = std::thread([this](){
		while (!m_stopped) {
			try {
				worker_loop();
			} catch (const std::exception &e) {
				logger.error("Indexer throws:", e.what());
				m_worker_ex = std::current_exception();
#ifndef NDEBUG
				break;
#endif
			}
		}

		logger.debug("Indexer thread exit");
	});
}

void Indexer::join()
{
	m_stopped = true;

	m_queue_cv.notify_all();

	if (m_worker.joinable())
		m_worker.join();

	if (m_worker_ex) {
		logger.info("Retrowing worker exception");
		std::rethrow_exception(m_worker_ex);
	}
}

bool Indexer::push(uint64_t paper_id)
{
#ifndef NDEBUG
	if (m_worker_ex) {
		logger.info("Retrowing indexer exception");
		std::rethrow_exception(m_worker_ex);
	}
#endif

	assert(paper_id > 0);

	logger.debug("Pushing", paper_id, "to indexer");

	{
		std::unique_lock lg(m_queue_lock);
		m_queue.push(paper_id);
	}

	m_queue_cv.notify_one();
	return true;
}

void Indexer::worker_loop()
{
	uint64_t paper_id = 0;

	while (!m_stopped) {
		if (paper_id)
			process_paper(paper_id);

		{
			std::unique_lock lg(m_queue_lock);
			paper_id = take_next_id();
		}

		if (paper_id == 0) {
			std::unique_lock lg(m_queue_lock);
			m_queue_cv.wait(lg);
			paper_id = take_next_id();
		}
	}
}

uint64_t Indexer::take_next_id()
{
	if (m_queue.empty())
		return 0;

	uint64_t paper_id = m_queue.front();
	m_queue.pop();
	return paper_id;
}

void Indexer::process_paper(uint64_t paper_id)
{
	std::string content = get_content(paper_id);

	if (content.empty()) {
		logger.warning("No content extracted for paper", paper_id);
		return;
	}

	index_content(paper_id, content);
}

std::string Indexer::get_content(uint64_t paper_id)
{
	assert(paper_id > 0);
	assert(!m_config.content_extract_cmd.empty());

	std::string cmd = m_config.content_extract_cmd;
	cmd += " ";
	cmd += m_config.storage_dir + "/" + std::to_string(paper_id);

	std::string content;
	logger.debug("Spawning", cmd);

	FILE *fp = popen(cmd.c_str(), "r");
	THROW_IF(fp == nullptr);

	try {
		char buf[256] = {0};
		while (fgets(buf, sizeof(buf) - 1, fp))
			content += buf;
	} catch (std::exception &ex) {
		pclose(fp);
		throw ex;
	}

	pclose(fp);

	logger.debug("Finished", cmd);
	// skipping rc check as we are ignoring sigchild

	return content;
}

void Indexer::index_content(uint64_t paper_id, const std::string &content)
{
	assert(paper_id > 0);
	assert(!content.empty());

	Transaction transaction(m_db);

	PapersIndex::UpdateContent index(m_db);
	index.run(paper_id, content);

	Papers::UpdateIndexDate paper(m_db);
	paper.paper_id = paper_id;
	paper.run();

	transaction.commit();
}

void Indexer::ignore_sigchld()
{
	int rc;

	struct sigaction sact = {};
	sact.sa_flags = 0;
	sact.sa_handler = SIG_IGN;

	rc = sigemptyset(&sact.sa_mask);
	THROW_IF(rc < 0);

	logger.debug("Ignoring signal", strsignal(SIGCHLD));
	rc = sigaction(SIGCHLD, &sact, nullptr);
	THROW_IF(rc < 0);
}

