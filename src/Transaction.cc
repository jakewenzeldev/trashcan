#include <cassert>

#include "Transaction.hh"
#include "Database.hh"
#include "Statement.hh"
#include "Logger.hh"

Transaction::Transaction(Database &db)
: m_db(db)
, m_commited(false)
{ 
	logger.debug("Starting transaction");
	m_db.exec("BEGIN TRANSACTION;");
}

Transaction::~Transaction()
{
	if (m_commited)
		return;

	// FIXME: it may throw
	logger.debug("Rolling back transaction");

	Statement stmt(m_db, "ROLLBACK;");
	stmt.step();
}

void Transaction::commit()
{
	logger.debug("Commiting transaction");

	Statement stmt(m_db, "COMMIT;");
	stmt.step();

	m_commited = true;
}
