BEGIN TRANSACTION;

ALTER TABLE Papers ADD COLUMN url TEXT;
ALTER TABLE Papers ADD COLUMN arxiv TEXT;
ALTER TABLE Papers ADD COLUMN notes TEXT;

DROP TABLE IF EXISTS NewPapersIndex;

CREATE VIRTUAL TABLE NewPapersIndex
USING fts4(
	paper_id REFERENCES Papers(paper_id),
	title,
	authors,
	year, 
	doi,
	isbn,
	arxiv,
	notes,
	content
);

INSERT INTO NewPapersIndex(
	paper_id,
	title,
	authors,
	year, 
	doi,
	isbn,
	content
)
SELECT paper_id, title, authors, year, doi, isbn, content
FROM PapersIndex;

DROP TABLE PapersIndex;

ALTER TABLE NewPapersIndex RENAME TO PapersIndex;

COMMIT;
